package me.alejnp.tvseriesalbum.adapter;

import java.util.List;

import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.model.objects.Character;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CharactersAdapter extends ArrayAdapter<Character> {

	public CharactersAdapter(Context context, List<Character> data) {
		super(context, R.layout.adapter_characters_item, data);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = null;
		
		if(convertView != null) {
			v = convertView;
		} else {
			v = LayoutInflater.from(getContext()).inflate(R.layout.adapter_characters_item, parent, false);
		}
		
		return updateView(v, getItem(position));
	}

	
	public View updateView(View v, Character e) {
		TextView name = (TextView) v.findViewById(R.id.list_item_value_name);
		name.setText(e.name);
		
		TextView airdate = (TextView) v.findViewById(R.id.list_item_value_played_by);
		airdate.setText(e.playedBy);
		
		return v;
	}

}
