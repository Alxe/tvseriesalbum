package me.alejnp.tvseriesalbum.adapter;

import java.io.IOException;
import java.util.List;

import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.model.objects.Character;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CharacterViewAdapter extends PagerAdapter {

	private final Context context;

	private final List<Character> list;

	public CharacterViewAdapter(Context context, List<Character> list) {
		this.context = context;
		this.list = list;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View v = LayoutInflater.from(context).inflate(
				R.layout.fragment_characters_view_item, container, false);

		Character element = list.get(position);

		Resources res = context.getResources();

		TextView name = (TextView) v
				.findViewById(R.id.fragment_character_view_name);

		name.setText(element.name);

		ImageView image = (ImageView) v
				.findViewById(R.id.fragment_character_view_image);

		try {
			image.setImageBitmap(BitmapFactory.decodeStream(res.getAssets()
					.open(element.photo)));
		} catch (IOException e) {
			image.setImageResource(R.drawable.index_icon_tardis);
		}

		container.addView(v);

		return v;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View v, Object obj) {
		return (v == obj);
	}

}