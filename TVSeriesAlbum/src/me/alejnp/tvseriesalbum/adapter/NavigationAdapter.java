package me.alejnp.tvseriesalbum.adapter;

import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.adapter.NavigationAdapter.NavigationItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NavigationAdapter extends ArrayAdapter<NavigationItem> {
	public static class NavigationItem {
		public final int textRes;
		
		public final int imageRes;
		
		public final Class<?> fragmentToLaunch;
		
		public NavigationItem(int textRes, int imageRes, Class<?> fragmentToLaunch) {
			this.textRes = textRes;
			this.imageRes = imageRes;
			this.fragmentToLaunch = fragmentToLaunch;
		}
	}

	private final Context context;
	
	private final NavigationItem[] data;
	
	public NavigationAdapter(Context context, NavigationItem[] data) {
		super(context, R.layout.adapter_navigation_item, data);
		
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(R.layout.adapter_navigation_item, null, true);
		
		NavigationItem data = this.data[position];
		
		TextView textView = (TextView) view.findViewById(R.id.navigation_item_text);
		textView.setText(data.textRes);
		textView.setCompoundDrawablesWithIntrinsicBounds(data.imageRes, 0, 0, 0);
		
		return view;
	}
}
