package me.alejnp.tvseriesalbum.adapter;

import java.util.List;

import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.model.objects.Episode;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EpisodesAdapter extends ArrayAdapter<Episode> {

	public EpisodesAdapter(Context context, List<Episode> data) {
		super(context, R.layout.adapter_episodes_item, data);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = null;
		
		if(convertView != null) {
			v = convertView;
		} else {
			v = LayoutInflater.from(getContext()).inflate(R.layout.adapter_episodes_item, parent, false);
		}
		
		return updateView(v, getItem(position));
	}

	
	public View updateView(View v, Episode e) {
		TextView name = (TextView) v.findViewById(R.id.list_item_value_name);
		name.setText(e.name);
		
		TextView airdate = (TextView) v.findViewById(R.id.list_item_value_airdate);
		airdate.setText(e.airdate);
		
		return v;
	}

}
