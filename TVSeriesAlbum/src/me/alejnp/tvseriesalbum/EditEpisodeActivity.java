package me.alejnp.tvseriesalbum;

import me.alejnp.tvseriesalbum.database.DatabaseHelper;
import me.alejnp.tvseriesalbum.model.mapper.EpisodeMapper;
import me.alejnp.tvseriesalbum.model.objects.Episode;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditEpisodeActivity extends Activity {
	private EditText txtName;
	
	private EditText txtDate;
	
	private Button btnSend;
	
	private Episode episode;
	
	private EpisodeMapper episodeMapper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_episode);
		
		episodeMapper = new EpisodeMapper(
				new DatabaseHelper(getApplicationContext()));
		
		updateView();
		defautValues();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_empty, menu);
		return true;
	}
	
	public void updateView() {
		txtName = (EditText) findViewById(R.id.edit_episode_name_text);
		txtDate = (EditText) findViewById(R.id.edit_episode_date_text);
		btnSend = (Button) findViewById(R.id.edit_episode_send);
		
		final Activity activity = this;
		
		btnSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Episode e = new Episode(Long.valueOf(-1),
						txtName.getText().toString(),
						txtDate.getText().toString());
				
				episodeMapper.open();
				if(episode == null) {
					episodeMapper.insert(e);
				} else {
					episodeMapper.update(episode.id, e);
				}
				episodeMapper.close();
				
				activity.finish();
			}
		});
	}
	
	public void defautValues() {
		Bundle extra = getIntent().getExtras();
		
		if(extra != null) {
			if(extra.containsKey("episode")) {
				this.episode = (Episode) extra.get("episode");
				
				txtName.setText(this.episode.name);
				txtDate.setText(this.episode.airdate);
			}
		}
	}

}
