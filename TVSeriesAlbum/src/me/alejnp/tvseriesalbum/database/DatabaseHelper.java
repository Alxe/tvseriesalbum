package me.alejnp.tvseriesalbum.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import me.alejnp.tvseriesalbum.model.mapper.CharacterMapper;
import me.alejnp.tvseriesalbum.model.mapper.EpisodeMapper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	/** 
	 * Database Version 
	 */
	private static final Integer VERSION = 1;
	
	/**
	 * Database Name
	 */
	private static final String SCHEMA = "TV_Shows";
	
	private static final String CREATE_EPISODES_TABLE =
			"CREATE TABLE `" + EpisodeMapper.TABLE_NAME + "` (" +
				"`" + EpisodeMapper.KEY_ID + "` INTEGER PRIMARY KEY," +
				"`" + EpisodeMapper.KEY_NAME + "` TEXT NOT NULL," +
				"`" + EpisodeMapper.KEY_AIRDATE + "` TEXT NOT NULL DEFAULT CURRENT_DATE)";
	
	private static final String CREATE_CHARACTERS_TABLE =
			"CREATE TABLE `" + CharacterMapper.TABLE_NAME + "` (" +
				"`" + CharacterMapper.KEY_ID + "` INTEGER PRIMARY KEY," +
				"`" + CharacterMapper.KEY_NAME + "` TEXT NOT NULL," +
				"`" + CharacterMapper.KEY_PHOTO + "` TEXT NOT NULL DEFAULT 'default.png'," +
				"`" + CharacterMapper.KEY_DESCRIPTION + "` TEXT NOT NULL DEFAULT 'Lorem Ipsum Dolors Amet'," +
				"`" + CharacterMapper.KEY_PLAYED_BY + "` TEXT NOT NULL DEFAULT 'n/a')";
	
	private static final String DROP_EPISODES_TABLE = 
			"DROP TABLE IF EXISTS `" + EpisodeMapper.TABLE_NAME + "`";
	
	private static final String DROP_CHARACTERS_TABLE = 
			"DROP TABLE IF EXISTS `" + CharacterMapper.TABLE_NAME + "`";
	
	private final Context context;

	public DatabaseHelper(Context context) {
		super(context, SCHEMA, null, VERSION);
		
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_EPISODES_TABLE);
		db.execSQL(CREATE_CHARACTERS_TABLE);
		
		for(String sql : getStringListFromFile("insert_db.sql")) {
			db.execSQL(sql);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DROP_EPISODES_TABLE);
		db.execSQL(DROP_CHARACTERS_TABLE);
		
		onCreate(db);
	}
	
	private String[] getStringListFromFile(String src) {
		List<String> list = new ArrayList<String>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(context.getAssets().open(src)));
			
			String line = null;
			while((line = br.readLine()) != null) {
				list.add(line);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch(NullPointerException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return list.toArray(new String[list.size()]);
	}
}
