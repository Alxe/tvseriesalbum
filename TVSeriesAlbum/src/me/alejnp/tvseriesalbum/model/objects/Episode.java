package me.alejnp.tvseriesalbum.model.objects;

import me.alejnp.common.template.Builder;
import android.os.Parcel;
import android.os.Parcelable;


public class Episode implements Parcelable {
	public static class EpisodeBuilder implements Builder<Episode> {
		private Long id;
		
		private String name;
		
		private String airdate;
		
		public EpisodeBuilder(Long id) {
			this.id = id;
		}

		public EpisodeBuilder id(Long id) {
			this.id = id; return this;
		}
		
		public EpisodeBuilder name(String name) {
			this.name = name; return this;
		}
		
		public EpisodeBuilder airdate(String airdate) {
			this.airdate = airdate; return this;
		}
		
		@Override
		public Episode build() {
			return new Episode(id, name, airdate);
		}
		
	}
	
	public static final Parcelable.Creator<Episode> CREATOR =
			new Parcelable.Creator<Episode>() {

				@Override
				public Episode createFromParcel(Parcel source) {
					return new Episode(source);
				}

				@Override
				public Episode[] newArray(int size) {
					return new Episode[size];
				}
		
	};
	
	public final Long id;
	
	public final String name;
	
	public final String airdate;
	
	public Episode(Long id, String name, String airdate) {
		this.id = id;
		this.name = name;
		this.airdate = airdate;
	}
	
	public Episode(Parcel in) {
		this.id = in.readLong();
		this.name = in.readString();
		this.airdate = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(airdate);
	}
}
