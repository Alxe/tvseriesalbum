package me.alejnp.tvseriesalbum.model.objects;

import me.alejnp.common.template.Builder;

public class Character {
	public static class CharacterBuilder implements Builder<Character> {
		private Long id;
		
		private String name, photo, description, playedBy;
		
		public CharacterBuilder() {
			
		}
		
		public CharacterBuilder id(Long id) {
			this.id = id; return this;
		}
		
		public CharacterBuilder name(String name) {
			this.name = name; return this;
		}
		
		public CharacterBuilder photo(String photo) {
			this.photo = photo; return this;
		}
		
		public CharacterBuilder description(String description) {
			this.description = description; return this;
		}
		
		public CharacterBuilder playedBy(String playedBy) {
			this.playedBy = playedBy; return this;
		}

		@Override
		public Character build() {
			return new Character(id, name, photo, description, playedBy);
		}
	}
	
	public final Long id;
	
	public final String name, photo, description, playedBy;
	
	public Character(Long id, String name, String photo, String description, String playedBy) {
		this.id = id;
		this.name = name;
		this.photo = photo;
		this.description = description;
		this.playedBy = playedBy;
	}
}
