package me.alejnp.tvseriesalbum.model.mapper;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.alejnp.common.template.AndroidDataMapper;
import me.alejnp.tvseriesalbum.database.DatabaseHelper;
import me.alejnp.tvseriesalbum.model.objects.Episode;
import me.alejnp.tvseriesalbum.model.objects.Episode.EpisodeBuilder;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EpisodeMapper implements AndroidDataMapper<Long, Episode> {

	public static final String TABLE_NAME = "Episodes";
	
	public static final String 
		KEY_ID = "_id",
		KEY_NAME = "Name",
		KEY_AIRDATE = "Original_Airdate";
	
	private final DatabaseHelper dba;
	
	private SQLiteDatabase db;
	
	public EpisodeMapper(DatabaseHelper dba) {
		this.dba = dba;
	}
	
	public EpisodeMapper open() {
		if(db == null || !db.isOpen()) {
			db = dba.getWritableDatabase(); 	
		}
		
		return this;
	}
	
	public EpisodeMapper close() {
		if(db != null && db.isOpen()) {
			db.close();
			db = null;
		} 
		
		return this;
	}
	
	@Override
	public boolean insert(Episode element) {
		boolean success = false;
		
		ContentValues values = new ContentValues();
		
		values.put(KEY_NAME, element.name);
		values.put(KEY_AIRDATE, (element.airdate != null) ? element.airdate : 
			DateFormat.getInstance().format(new Date()));
		
		if(db.insert(TABLE_NAME, null, values) != -1) {
			success = true;
		}
		
		return success;
	}

	@Override
	public Episode retrieve(Long id) {
		Episode episode = null;
		
		Cursor c = db.query(true, TABLE_NAME, new String[] { KEY_ID, KEY_NAME, KEY_AIRDATE }, 
			String.format("%1$s = %2$s", KEY_NAME, id), null, null, null, null, null);
		
		if(c.moveToFirst()) {
			episode = buildFromCursor(c);
		}
	
		return episode;
	}

	@Override
	public List<Episode> retrieveAll() {
		List<Episode> list = new ArrayList<Episode>();
		
		Cursor c = retrieveAllAsCursor();
		
		while(c.moveToNext()) {
			list.add(buildFromCursor(c));
		}
		
		return list;
	}
	
	@Override
	public Cursor retrieveAllAsCursor() {
		Cursor c = db.query(true, TABLE_NAME, new String[] { KEY_ID, KEY_NAME, KEY_AIRDATE }, 
					null, null, null, null, null, null);
		
		return c;
	}

	@Override
	public boolean update(Long id, Episode element) {
		boolean success = false;
		
		ContentValues values = new ContentValues();
		
		values.put(KEY_NAME, element.name);
		values.put(KEY_AIRDATE, element.airdate);
		
		if(db.update(TABLE_NAME, values, String.format("%1$s = %2$s", KEY_ID, id), null) > 0) {
			success = true;
		}

		return success;
	}

	@Override
	public boolean delete(Long id) {		
		boolean success = false;
		
		if(db.delete(TABLE_NAME, String.format("%1$s = %2$s", KEY_ID, id), null) > 0) {
			success = true;
		}
		
		return success;
	}
	
	private Episode buildFromCursor(Cursor c) {
		Episode episode = new EpisodeBuilder(c.getLong(0))
			.name(c.getString(1))
			.airdate(c.getString(2))
			.build();
		
		return episode;
	}
}
