package me.alejnp.tvseriesalbum.model.mapper;

import java.util.ArrayList;
import java.util.List;

import me.alejnp.common.template.DataMapper;
import me.alejnp.tvseriesalbum.database.DatabaseHelper;
import me.alejnp.tvseriesalbum.model.objects.Character;
import me.alejnp.tvseriesalbum.model.objects.Character.CharacterBuilder;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CharacterMapper implements DataMapper<Long, Character> {

	public static final String TABLE_NAME = "Characters";
	
	public static final String
		KEY_ID = "_id",
		KEY_NAME = "Name",
		KEY_PHOTO = "Photo",
		KEY_DESCRIPTION = "Description",
		KEY_PLAYED_BY = "Played_By";
	
	private final DatabaseHelper dba;
	
	private SQLiteDatabase db;
	
	public CharacterMapper(DatabaseHelper dba) {
		this.dba = dba;
	}
	
	public boolean open() {
		if(db == null || !db.isOpen()) {
			db = dba.getWritableDatabase();
		}
		
		return db.isOpen();
	}
	
	public boolean close() {
		if(db != null && db.isOpen()) {
			db.close();
			db = null;
		} 
		
		return (db == null);
	}
	
	
	@Override
	public boolean insert(Character element) {
		boolean success = false;
		
		ContentValues values = new ContentValues();
		
		values.put(KEY_NAME, element.name);
		values.put(KEY_PHOTO, element.photo);
		values.put(KEY_PLAYED_BY, element.playedBy);
		
		if(db.insert(TABLE_NAME, null, values) != -1) {
			success = true;
		}
		
		return success;
	}

	@Override
	public Character retrieve(Long id) {
		Character character = null;
		
		Cursor c = db.query(true, TABLE_NAME, new String[] { KEY_ID, KEY_NAME, KEY_PHOTO, KEY_PLAYED_BY }, 
				String.format("%1$s = %2$s", KEY_NAME, id), null, null, null, null, null);
		
		if(c.moveToFirst()) {
			character = buildFromCursor(c);
		}
		
		return character;
	}

	@Override
	public List<Character> retrieveAll() {
		List<Character> list = new ArrayList<Character>();
		
		Cursor c = retrieveAllAsCursor();
		while(c.moveToNext()) {
			list.add(buildFromCursor(c));
		}
		
		return list;
	}
	
	public Cursor retrieveAllAsCursor() {
		return db.query(true, TABLE_NAME, new String[] { KEY_ID, KEY_NAME, KEY_PHOTO, KEY_PLAYED_BY }, 
				null, null, null, null, null, null);
	}

	@Override
	public boolean update(Long id, Character element) {
		boolean success = false;
		
		ContentValues values = new ContentValues();
		
		values.put(KEY_NAME, element.name);
		values.put(KEY_PHOTO, element.photo);
		values.put(KEY_PLAYED_BY, element.playedBy);
		
		if(db.update(TABLE_NAME, values, String.format("%1$s = %2$s", KEY_ID, id), null) > 0) {
			success = true;
		}

		return success;
	}

	@Override
	public boolean delete(Long id) {
		boolean success = false;
		
		if(db.delete(TABLE_NAME, String.format("%1$s = %2$s", KEY_ID, id), null) > 0) {
			success = true;
		}
		
		return success;
	}

	private Character buildFromCursor(Cursor c) {
		Character character = new CharacterBuilder()
			.id(c.getLong(0))
			.name(c.getString(1))
			.photo(c.getString(2))
			.playedBy(c.getString(3))
			.build();
		
		return character;
	}
}
