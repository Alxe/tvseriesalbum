package me.alejnp.tvseriesalbum;

import me.alejnp.tvseriesalbum.adapter.NavigationAdapter;
import me.alejnp.tvseriesalbum.adapter.NavigationAdapter.NavigationItem;
import me.alejnp.tvseriesalbum.database.DatabaseHelper;
import me.alejnp.tvseriesalbum.fragment.CharactersFragment;
import me.alejnp.tvseriesalbum.fragment.CharactersFragment.FragmentTabListener;
import me.alejnp.tvseriesalbum.fragment.CharactersListFragment;
import me.alejnp.tvseriesalbum.fragment.CharactersViewFragment;
import me.alejnp.tvseriesalbum.fragment.EpisodesFragment;
import me.alejnp.tvseriesalbum.fragment.IndexFragment;
import me.alejnp.tvseriesalbum.fragment.PreferencesFragment;
import me.alejnp.tvseriesalbum.model.mapper.CharacterMapper;
import me.alejnp.tvseriesalbum.model.mapper.EpisodeMapper;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements OnItemClickListener {
//	private FrameLayout frmContent;
	
	private SharedPreferences prefs;
	
//	private Menu menu;
	
	private ListView lstNavigation;
	
	private ListAdapter adapter;
	
	private DrawerLayout drlNavigationDrawer;
	
	private ActionBarDrawerToggle actionBarDrawerToggle;	
	
	private FragmentManager frgManager;
	
	private Fragment loadedFragment;
	
	private Fragment[] fragmentCache;
	
	private EpisodeMapper episodeMapper;
	
	private CharacterMapper characterMapper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		frgManager = getSupportFragmentManager();
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		
		DatabaseHelper dba = new DatabaseHelper(getApplicationContext());
		episodeMapper = new EpisodeMapper(dba);
		characterMapper = new CharacterMapper(dba);
		
		updateViews(savedInstanceState);
		initViews(savedInstanceState);
		defaultBehaviour(savedInstanceState);
	}

	private void updateViews(Bundle bundle) {
//		frmContent = (FrameLayout) findViewById(R.id.frmContent);
		lstNavigation = (ListView) findViewById(R.id.lstNavigation);
		drlNavigationDrawer = (DrawerLayout) findViewById(R.id.drlNavigationDrawer);

	}
	
	private void initViews(Bundle bundle) {
		// ListView adapter creation and "feeding" of data.
		adapter = new NavigationAdapter(getApplicationContext(), 
				new NavigationItem[] {
			new NavigationItem(R.string.navigation_list_index, R.drawable.ic_action_send_now, IndexFragment.class),
			new NavigationItem(R.string.navigation_list_episodes, R.drawable.ic_action_send_now, EpisodesFragment.class),
			new NavigationItem(R.string.navigation_list_characters, R.drawable.ic_action_send_now, CharactersFragment.class),
			new NavigationItem(R.string.navigation_list_preferences, R.drawable.ic_action_send_now, PreferencesFragment.class)
		});
		
		// Cache initialization
		fragmentCache = new Fragment[adapter.getCount()];
		
		// ListView adapter assembly.
		lstNavigation.setAdapter(adapter);
		lstNavigation.setOnItemClickListener(this);
		
		// Toggle creation.
		actionBarDrawerToggle = 
				new ActionBarDrawerToggle(this, drlNavigationDrawer, R.drawable.ic_navigation_drawer,
				R.string.activity_title_on_drawer_opened, R.string.activity_title_on_drawer_closed) {

					private ActionBar ab = getActionBar();
					@Override
					public void onDrawerSlide(View drawerView, float slideOffset) {
						int navMode = ActionBar.NAVIGATION_MODE_STANDARD;
						if(slideOffset <= 0.0 && loadedFragment != null && 
								loadedFragment instanceof CharactersFragment) {
							navMode = ActionBar.NAVIGATION_MODE_TABS;
						}
						
						ab.setNavigationMode(navMode);
					}
			
					@Override
					public void onDrawerClosed(View drawerView) {
						ab.setTitle(R.string.activity_title_on_drawer_closed);
						
						invalidateOptionsMenu();
					}

					@Override
					public void onDrawerOpened(View drawerView) {
						ab.setTitle(R.string.activity_title_on_drawer_opened);
						
						invalidateOptionsMenu();
					}
			
		};
	
		// Activate home button as clickable.
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Tab listTab = getActionBar().newTab();
		listTab.setText(R.string.characters_tab_name_list);
		listTab.setTabListener(new FragmentTabListener(
				new CharactersListFragment()));
		
		Tab viewTab = getActionBar().newTab();
		viewTab.setText(R.string.characters_tab_view_list);
		viewTab.setTabListener(new FragmentTabListener(
				new CharactersViewFragment()));
		
		getActionBar().addTab(listTab, true);
		getActionBar().addTab(viewTab, false);
		
		// Toggle and actionbar link
		drlNavigationDrawer.setDrawerListener(actionBarDrawerToggle);
	}
	
	/**
	 * Default behaviour to have on Activity created.
	 * @param bundle savedStateInstance of Activity
	 */
	private void defaultBehaviour(Bundle bundle) {
		// Overriding startup fragment: Checks if it's enabled, and proceeds to load selected index if found. Else, load IndexFragment.
		int startFragmentPosition = 0;
		
		Resources res = getResources();
		
		boolean overrideFragmentLocation = prefs.getBoolean(
				res.getString(R.string.preferences_default_fragment_override_key), false);
		
		if(overrideFragmentLocation) {
			String indexFragmentLocation = IndexFragment.class.getName();
			
			String startFragmentLocation = prefs.getString(
					res.getString(R.string.preferences_default_fragment_override_location_key), indexFragmentLocation);
			
			if(!startFragmentLocation
					.equals(indexFragmentLocation)) {
							
				for(int i = 0; i < adapter.getCount(); i++) {
					NavigationItem e = (NavigationItem) adapter.getItem(i);
					
					if(e.fragmentToLaunch.getName()
							.equals(startFragmentLocation)) {
						
						startFragmentPosition = i;
						
						break;
					}
				}
			}
		}
		
		loadOnNavigationSelected(startFragmentPosition);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_empty, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(actionBarDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		loadOnNavigationSelected(position);
	}
	
	
	private void loadOnNavigationSelected(int position) {
//		getMenuInflater().inflate(R.menu.menu_empty, menu);
		
		if(fragmentCache[position] == null) {
			try {
				fragmentCache[position] = (Fragment) ((NavigationItem) lstNavigation.getItemAtPosition(position))
						.fragmentToLaunch.newInstance();

				Bundle fragmentArgs = new Bundle();
				
				fragmentCache[position].setArguments(fragmentArgs);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		Fragment fragment = fragmentCache[position];
		
		frgManager.beginTransaction()
			.replace(R.id.frmContent, (loadedFragment = fragment))
			.commit();
		
		
		lstNavigation.setItemChecked(position, true);
		
		drlNavigationDrawer.closeDrawer(lstNavigation);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		actionBarDrawerToggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	public EpisodeMapper getEpisodeMapper() {
		return episodeMapper;
	}

	public CharacterMapper getCharacterMapper() {
		return characterMapper;
	}
}
