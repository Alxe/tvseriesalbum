package me.alejnp.tvseriesalbum.fragment;

import me.alejnp.tvseriesalbum.R;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CharactersFragment extends Fragment {
	public static final class FragmentTabListener implements TabListener {
		private final android.app.Fragment fragment;
		
		public FragmentTabListener(android.app.Fragment fragment) {
			this.fragment = fragment;
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction trans) {
			// do nothing
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction trans) {
			trans.replace(R.id.characters_content, fragment);
			
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction trans) {
			trans.remove(fragment);
			
		}
	}

//	private Tab listTab, viewTab;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
//		ActionBar ab = getActivity().getActionBar();
//		
//		listTab = ab.getTabAt(0);
//		viewTab = ab.getTabAt(1);
		
		return inflater.inflate(R.layout.fragment_characters, container, false);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
}
