package me.alejnp.tvseriesalbum.fragment;

import me.alejnp.tvseriesalbum.R;
import android.app.FragmentManager;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PreferencesFragment extends Fragment {
	public static class InnerPreferencesFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			
			
			addPreferencesFromResource(R.layout.fragment_preferences);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_replaceable, container,
				false);

		FragmentManager fragmentManager = getActivity().getFragmentManager();

		fragmentManager.beginTransaction()
				.replace(R.id.frame_content, new InnerPreferencesFragment())
				.commit();

		return v;
	}

}
