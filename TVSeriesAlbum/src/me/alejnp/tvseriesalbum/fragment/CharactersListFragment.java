package me.alejnp.tvseriesalbum.fragment;

import java.util.ArrayList;
import java.util.List;

import me.alejnp.tvseriesalbum.MainActivity;
import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.adapter.CharactersAdapter;
import me.alejnp.tvseriesalbum.model.mapper.CharacterMapper;
import me.alejnp.tvseriesalbum.model.objects.Character;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class CharactersListFragment extends Fragment {
	private ListView listCharacters;
	
	private CharacterMapper mapper;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_characters_list, container, false);
		
		mapper = ((MainActivity) getActivity())
					.getCharacterMapper();
		
		listCharacters = (ListView) v.findViewById(R.id.fragment_characters_list);
		
		ListAdapter adapter = new CharactersAdapter(getActivity(), new ArrayList<Character>());
		
		listCharacters.setAdapter(adapter);
		
//		listCharacters.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
//				Tab detailsTab = getActivity().getActionBar().getTabAt(1);
//			}
//		});
		
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		
		mapper.open();
		
		List<Character> elements = mapper.retrieveAll();
		
		@SuppressWarnings("unchecked")
		ArrayAdapter<Character> adapter = (ArrayAdapter<Character>) listCharacters.getAdapter();
		
		adapter.clear();
		adapter.addAll(elements);
		adapter.notifyDataSetChanged();
		
		mapper.close();
	}
	
}