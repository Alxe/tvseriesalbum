package me.alejnp.tvseriesalbum.fragment;

import java.util.List;

import me.alejnp.tvseriesalbum.MainActivity;
import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.adapter.CharacterViewAdapter;
import me.alejnp.tvseriesalbum.model.mapper.CharacterMapper;
import me.alejnp.tvseriesalbum.model.objects.Character;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CharactersViewFragment extends Fragment {
	 private ViewPager pagerCharacters;
	
	private CharacterMapper mapper;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_characters_view, container, false);
		
		mapper = ((MainActivity) getActivity())
					.getCharacterMapper();
		
		pagerCharacters = (ViewPager) v.findViewById(R.id.fragment_character_details_pager);
		pagerCharacters.setCurrentItem(0);
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		
		mapper.open();
		List<Character> elements = mapper.retrieveAll();
		mapper.close();
		
		PagerAdapter adapter = new CharacterViewAdapter(getActivity(), elements);
		
		pagerCharacters.setAdapter(adapter);
		
		
	}
}
	
