package me.alejnp.tvseriesalbum.fragment;

import java.util.ArrayList;
import java.util.List;

import me.alejnp.tvseriesalbum.EditEpisodeActivity;
import me.alejnp.tvseriesalbum.MainActivity;
import me.alejnp.tvseriesalbum.R;
import me.alejnp.tvseriesalbum.adapter.EpisodesAdapter;
import me.alejnp.tvseriesalbum.model.mapper.EpisodeMapper;
import me.alejnp.tvseriesalbum.model.objects.Episode;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class EpisodesFragment extends Fragment {
	private ListView lstEpisodes;

	private EpisodeMapper episodeMapper;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate base view.
		View v = inflater.inflate(R.layout.fragment_episodes, container, false);
		
		// Update references.
		lstEpisodes = (ListView) v.findViewById(R.id.fragment_episodes_list);
		episodeMapper = ((MainActivity) getActivity()).getEpisodeMapper();

		// Initialise list adapter and it's data.
		List<Episode> data = episodeMapper.open().retrieveAll();
		episodeMapper.close();
		
		lstEpisodes.setAdapter(new EpisodesAdapter(getActivity(), data));
		
		lstEpisodes.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
				final Context ctx = getActivity();
				
				final Episode e = (Episode) adapter.getItemAtPosition(position);
				
				Intent itnt = new Intent(ctx, EditEpisodeActivity.class);
				itnt.putExtra("episode", e);
				
				ctx.startActivity(itnt);	
			}
		});
		
		lstEpisodes.setMultiChoiceModeListener(new MultiChoiceModeListener() {
			
			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				return false;
			}
			
			@Override
			public void onDestroyActionMode(ActionMode mode) {
				return;
			}
			
			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				mode.getMenuInflater()
					.inflate(R.menu.menu_episodes_am, menu);
				
				return true;
			}
			
			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {				
				switch(item.getItemId()) {
				case R.id.menu_episodes_remove:
					List<Episode> elements = getCheckedEpisodes(lstEpisodes, true);
					
					EpisodeMapper mapper = ((MainActivity) getActivity()).getEpisodeMapper();
					
					mapper.open();
					
					for(Episode e : elements) { mapper.delete(e.id); }
					
					mapper.close();
					
					updateAdapterData();
					
					break;
				}
				
				return true;
			}
			
			@Override
			public void onItemCheckedStateChanged(ActionMode mode, int position,
					long id, boolean checked) {
				mode.setTitle(String.format(
						"%s / %s", lstEpisodes.getCheckedItemCount(), lstEpisodes.getCount()));
				
			}
		});
		
		
		// Final settings
		setHasOptionsMenu(true);

		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.menu_episodes, menu);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		
		menu.findItem(R.id.menu_episodes_add)
			.setOnMenuItemClickListener(new OnMenuItemClickListener() {
		
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			final Context ctx = getActivity();
			
			Intent itnt = new Intent(ctx, EditEpisodeActivity.class);
			
			ctx.startActivity(itnt);
			
			return true;
		}
	});
	}

	@Override
	public void onResume() {
		super.onResume();
		
		updateAdapterData();
	}

	private void updateAdapterData() {
		// Refresh data.
		List<Episode> data = episodeMapper.open().retrieveAll();
		episodeMapper.close();

		// Fetch adapter reference.
		EpisodesAdapter adapter = (EpisodesAdapter) lstEpisodes.getAdapter();

		// Update data.
		adapter.clear();
		adapter.addAll(data);
	}

	public List<Episode> getCheckedEpisodes(ListView list, boolean uncheck) {
		List<Episode> elements = new ArrayList<Episode>();
		
		SparseBooleanArray checked = list.getCheckedItemPositions();
		for(int i = 0; i < checked.size(); i++) {
			if(checked.valueAt(i)) {
				int position = checked.keyAt(i);
				
				if(uncheck) { list.setItemChecked(position, false); }
				
				elements.add((Episode) list.getItemAtPosition(position));
				
			}
		}
		
		return elements;
	}
}
