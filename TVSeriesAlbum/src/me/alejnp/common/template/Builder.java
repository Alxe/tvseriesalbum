package me.alejnp.common.template;

public interface Builder<T> {
	public T build();
}
