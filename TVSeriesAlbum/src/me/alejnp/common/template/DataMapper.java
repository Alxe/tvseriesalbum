package me.alejnp.common.template;

import java.util.List;

public interface DataMapper<K, V> {
	/**
	 * Inserts an element of type V into the data source.
	 * @param element
	 * @return
	 */
	public boolean insert(V element);
	
	/**
	 * Retrieves an element of type V from the data source given the element's ID.
	 * @param id The ID of the element, of type K.
	 * @return The element.
	 */
	public V retrieve(K id);
	
	/**
	 * Retrieves all elements of type T from the data source.
	 * @return The elements contained in a List of type T.
	 */
	public List<V> retrieveAll();
	
	/**
	 * Inserts an element of type V into the data source, returning false if not existant.
	 * @param id The ID of type K.
	 * @param element The element of type V.
	 * @return True if update is successful, false if not existant or error found.
	 */
	public boolean update(K id, V element);
	
	/**
	 * Deletes an element of a given ID and returns true if successful.
	 * @param id The ID of the element.
	 * @return True if successfull, false otherwise.
	 */
	public boolean delete(K id);
}
