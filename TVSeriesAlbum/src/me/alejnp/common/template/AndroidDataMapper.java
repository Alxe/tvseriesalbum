package me.alejnp.common.template;

import android.database.Cursor;

public interface AndroidDataMapper<K, V> extends DataMapper<K, V> {
	public Cursor retrieveAllAsCursor();
}
