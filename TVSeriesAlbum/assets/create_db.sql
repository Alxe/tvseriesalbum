DROP TABLE IF EXISTS `Episodes`
DROP TABLE IF EXISTS `Characters`
CREATE TABLE `Episodes` (`_id` INTEGER PRIMARY KEY, `Name` TEXT NOT NULL, `Original_Airdate` TEXT DEFAULT CURRENT_DATE NOT NULL)
CREATE TABLE `Characters` (`_id` INTEGER PRIMARY KEY, `Name` TEXT NOT NULL, `Photo` TEXT DEFAULT 'default.png' NOT NULL, `Description` TEXT DEFAULT 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel mauris eu tortor tincidunt laoreet sed nec dolor. Fusce aliquam.' NOT NULL, `Played_By` TEXT DEFAULT 'n/a' NOT NULL)
