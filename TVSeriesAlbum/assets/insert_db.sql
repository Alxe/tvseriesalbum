INSERT INTO `Episodes` (`Name`) VALUES ('Rose'); 
INSERT INTO `Episodes` (`Name`) VALUES ('The End of the World');
INSERT INTO `Episodes` (`Name`) VALUES ('The Unquiet Dead');
INSERT INTO `Episodes` (`Name`) VALUES ('Aliens of London');
INSERT INTO `Episodes` (`Name`) VALUES ('World War Three'); 
INSERT INTO `Episodes` (`Name`) VALUES ('Dalek');
INSERT INTO `Episodes` (`Name`) VALUES ('The Long Game');
INSERT INTO `Episodes` (`Name`) VALUES ('Father''s Day');
INSERT INTO `Episodes` (`Name`) VALUES ('The Empty Child');
INSERT INTO `Episodes` (`Name`) VALUES ('The Doctor Dances');
INSERT INTO `Episodes` (`Name`) VALUES ('Boom Town');
INSERT INTO `Episodes` (`Name`) VALUES ('Bad Wolf');
INSERT INTO `Episodes` (`Name`) VALUES ('The Partying of the Ways');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Ninth Doctor', 'ninth.png', 'Chistopher Eccleston');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Rose Tyler', 'rose.png', 'Billie Piper');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Jack Harkness', 'jack.png', 'John Barrowman');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Mickey Smith', 'mickey.png', 'Noel Clarke');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Jackie Tyler', 'jackie.png', 'Camille Coduri');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Tenth Doctor', 'tenth.png', 'David Tennant');
INSERT INTO `Characters` (`Name`, `Photo`, `Played_By`) VALUES ('Eleventh Doctor', 'eleventh.png', 'Matt Smith');